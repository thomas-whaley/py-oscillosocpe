# Py Oscillosocpe


## Oscilloscope emulator

Emulates a cathode ray tube oscilloscope

Oscilloscope converts voltage to a display offset

In this case the voltage is an audio signal

We get the voltage by converting a sound file to an array of waves

Then we use these waves as voltage and display them

More on the displaying of it later


Created for Python 3.7.3 on Windows 10

### TO RUN
   - Must have these packages installed
       - pygame
       - scipy
       - numpy