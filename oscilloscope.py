# Oscilloscope emulator
# Emulates a cathode ray tube oscilloscope
# Oscilloscope converts voltage to a display offset
# In this case the voltage is an audio signal
# We get the voltage by converting a sound file to an array of waves
# Then we use these waves as voltage and display them
# More on the displaying of it later

# Created for Python 3.7.3 on Windows 10

# TO RUN
#   - Must have these packages installed
#       - pygame
#       - scipy
#       - numpy

# File of sound file (must be wav):
import OpenFileDialog

file = OpenFileDialog.open_file()
if not file:
    quit()

import pygame
import time
import math
import pygame.gfxdraw
import numpy as np
# from scipy.ndimage import gaussian_filter1d


def split_list(array, n):
    # Splits list
    # If n is 0 it returns all the first indexes of a 2-D array
    # n = 0
    # foo = [[1, 2], [3, 4]]
    # split_list(n)
    # [1, 3]
    #
    # n = 1
    # split_list(n)
    # [2, 4]

    x = []
    for i in range(len(array)):
        if len(array[i]) > n:
            x.append(array[i][n])
    return x


def list_to_float(l):
    # Converts all things in list to floating point numbers
    if isinstance(l, list):
        for i in l:
            if isinstance(i, list):
                list_to_float(i)
            else:
                try:
                    i = float(i)
                except:
                    print(i)
                    raise TypeError("'l' variable in 'list_to_float' function expected list!")
        return i
    else:
        raise TypeError("'l' variable in 'list_to_float' function expected list!")


# def smooth_lines(list_pos, sigma=1):
#     # Smooth out points in a 2-D array as if there were a smooth line running through the points
#     smooth_pos = []
#     if isinstance(list_pos, list):
#         x = split_list(list_pos, 0)
#         y = split_list(list_pos, 1)
#         x1 = gaussian_filter1d(x, sigma)
#         y1 = gaussian_filter1d(y, sigma)
#         if len(x1) > len(y1):
#             length = len(y1)
#         elif len(x1) < len(y1):
#             length = len(x1)
#         else:
#             length = len(x1)
#         for i in range(length):
#             smooth_pos.append([x1[i], y1[i]])
#         return smooth_pos
#     else:
#         raise TypeError("Positions to smooth in 'smooth_lines' function expected list!")


class Oscilloscope:
    # Check file audio type
    if file.split("\\")[-1].split(".")[-1] != "wav":  # If the audio is not in wav type
        print("Audio file must be in 'wav' format")
        exit()  # Terminate script - critical error

    # Requires a path to an audio file

    def __init__(self, file):
        self.height = 720
        self.width = 1280
        # Initialise pygame
        pygame.mixer.init()
        pygame.init()
        # Set a pygame window
        self.window = pygame.display.set_mode((self.width, self.height), pygame.FULLSCREEN)
        pygame.mouse.set_visible(0)
        self.running = True
        # Create a pygame sound object
        self.sound = pygame.mixer.Sound(file)
        # Covert the pygame sound object to an array
        self.audio_array = pygame.sndarray.array(self.sound)
        # Create a list that will store instances of a class of dots
        self.dots = []
        # State the frequency that the music runs at
        self.frequency = 22050
        # State how auccurately the program draws the lines (lower is more accurate)
        self.accuracy = 0
        # State the amplitude (size) of the offset
        self.amplitude = 100

        self.roundness = 2

        self.blur = 0

    def events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:  # Check if the window has quit
                self.running = False
            elif event.type == pygame.KEYDOWN:
                # Get input
                if event.key == pygame.K_ESCAPE:
                    self.running = False

    def render(self):
        # Clear the screen
        if self.blur > 10:
            self.blur = 0
            self.window.fill((0, 0, 0))
        else:
            self.blur += 1
        if len(self.dots) > 1:
            # Draw lines between all points in the list
            pygame.draw.aalines(self.window, (0, 255, 0), False, self.dots)
        self.dots = []
        # Update the screen
        pygame.display.flip()

    def offset(self, l, r):
        # Offset diagram:
        # +--------------------------------------+
        # |                                      |
        # |   offset on y axis (right channel)   |
        # |                  ^                   |
        # |                  |        offset on  |
        # |                centre -->   x axis   |
        # |                             (left    |
        # |                            channel)  |
        # |                                      |
        # +--------------------------------------+
        # The left channel offsets the centre of the screen on the x axis (adding to the centre)
        # The right channel offsets the centre of the screen on the y axis (subtracting to the centre)
        return [(self.width / 2) + (l / self.amplitude), (self.height / 2) - (r / self.amplitude)]

    def loop(self):
        start = time.time()
        self.sound.play()
        d = pygame.time.Clock()
        frame = 0
        fps = []
        while self.running:
            d.tick()
            fps.append(d.get_fps())
            self.events()
            old_frame = frame
            # Get the current frame
            frame = self.frequency * (time.time() - start)
            delta_frame = frame - old_frame
            if self.accuracy < 1:
                self.accuracy = 1
            # Go through all of the frames since the last time it was called
            if frame < len(self.audio_array):
                for i in range(int(old_frame), int(frame), int(self.accuracy)):
                    self.dots.append(self.offset(self.audio_array[i][0], self.audio_array[i][1]))

            # Render
            self.render()
        # Deinitialise pygame
        pygame.quit()
        print("FPS: ", int(sum(fps) / len(fps)))


main = Oscilloscope(file)

main.loop()
