import tkinter as tk
from tkinter import filedialog


def open_file() -> str:
    root = tk.Tk()
    root.withdraw()

    file = filedialog.askopenfilename()
    return file
